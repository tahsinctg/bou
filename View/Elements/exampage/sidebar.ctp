<div class="col-md-3 sidebar">
                        <div class="list-group">
                            <a href="#" class="list-group-item active side-title">
                                <i class="glyphicon glyphicon-book" id="slide-submenu"></i> Lesson Material
                            </a>
                            <a href="#" class="list-group-item">
                                               <i class="fa fa-file"></i> Introduction to Business Studies
                                           </a>
                                           <a href="#" class="list-group-item">
                                               <i class="fa fa-file"></i> Value Proposition for Customers
                                           </a>
                                           <a href="#" class="list-group-item">
                                               <i class="fa fa-file"></i> Business Plans for a Freelance Business
                                           </a>
                                           <a href="#" class="list-group-item">
                                               <i class="fa fa-file"></i> Social Strategy Conversations & Communities
                                           </a>
                                       <div class="list-group-item">
                                           <!--<a href="#" class="">-->
                                               <i class="fa fa-file"></i> Assignment: Business and Enterprise Assignments
                                               <div class="rg-sec text-right">
                                                   <a class="cr-button cs-assignment">Assignment</a>
                                               </div>
                                           <!--</a>-->
                                       </div>
                                       <div class="list-group-item">
                                           <!--<a href="#" class="">-->
                                           <i class="fa fa-file"></i> Quiz Marketing
                                           <div class="rg-sec text-right">
                                               <a class="cr-button cs-assignment">Quiz</a>
                                           </div>
                                           <!--</a>-->
                                       </div>
                                           <a href="#" class="list-group-item">
                                               <i class="fa fa-file"></i> Lorem ipsum
                                           </a>
                        </div>           
                        <div class="list-group">
                            <a href="#" class="list-group-item active side-title">
                                <i class="glyphicon glyphicon-refresh"></i> Lesson Progress
                            </a>
                            <a href="#" class="list-group-item">
                                <div class="progress flat">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" id="progress-bar-width">40% Complete</div>
                                </div>
                                <p class="text-danger">Completion Rules: completed 0 out 1</p>
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="#" class="list-group-item active side-title">
                                <i class="glyphicon glyphicon-cog"></i> Lesson Operation
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="glyphicon glyphicon-print"></i> Print Unit
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="glyphicon glyphicon-comment"></i> Add Comment
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="glyphicon glyphicon-warning-sign"></i> Report an error
                            </a>
                            <a href="#" class="list-group-item">
                                <i class="glyphicon glyphicon-new-window"></i> Open unit in popup window
                            </a>
                        </div>
                        <div class="list-group">
                            <a href="#" class="list-group-item active side-title">
                                <i class="glyphicon glyphicon-question-sign"></i> FAQ
                            </a>
                            <a href="#" class="list-group-item">
                                1. How to add JavaScript in HTML?
                            </a>
                            <a href="#" class="list-group-item">
                                2. What is the purpose of CSS3?
                            </a>
                        </div>
                    </div><!-- Left-sideber End -->
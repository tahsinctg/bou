<div class="col-md-3 sidebar">
                    <div class="side-title"><?php echo __('Course Session'); ?></div>
                    <div class="side-content">
                        <form class="form-horizontal" role="form">
                            <select class="form-control">
                              <option>30th Nov - 4th Dec 2014</option>
                              <option>14th Dec - 20th Dec 2014</option>
                            </select>
                            <button type="button" class="btn btn-primary full"><?php echo __('Join for free'); ?></button>
                        </form>  
                    </div>
                    <div class="side-title"><?php echo __('Course Duration'); ?></div>
                    <div class="side-content">
                        <ul class="list-unstyled">
                            <li><p><i class="fa fa-calendar"></i> 4 <?php echo __('Weeks'); ?></p></li>
                            <li><p><i class="fa fa-clock-o"></i> 3-4 <?php echo __('Hours per week'); ?></p></li>
                            <li><p><i class="fa fa-globe"></i> <?php echo __('Bengali'); ?></p></li>
                        </ul>
                    </div>
                    <div class="side-title"><?php echo __('Course Teacher'); ?></div>
                    <div class="side-content">
                        <div class="teacher-pic">
                            <?php echo $this->Html->image('/img/tech.jpg',array('id'=>'','alt'=>'')); ?>
                        </div>
                        <div class="teacher-desc">
                            <p class="name">Mr. Abdus Salam</p>
                            <p class="education">BUET</p>
                        </div>
                    </div>
                    <div class="side-title"><?php echo __('Calender'); ?></div>
                    <div class="side-content">
                        <div class="responsive-calendar">
                            <div class="controls">
                                <h4><span data-head-year></span> <span data-head-month></span></h4>
                            </div>
                            <div class="day-headers">
                              <div class="day header"><?php echo __('Mon'); ?></div>
                              <div class="day header"><?php echo __('Tue'); ?></div>
                              <div class="day header"><?php echo __('Wed'); ?></div>
                              <div class="day header"><?php echo __('Thu'); ?></div>
                              <div class="day header"><?php echo __('Fir'); ?></div>
                              <div class="day header"><?php echo __('Sat'); ?></div>
                              <div class="day header"><?php echo __('Sun'); ?></div>
                            </div>
                            <div class="days" data-group="days">
                              
                            </div>
                          </div>
                    </div>
                </div>
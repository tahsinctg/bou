<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

<!-- Mirrored from themes.3rdwavemedia.com/college-green/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 18 Nov 2014 08:48:33 GMT -->
<head>
    <title>Responsive website template for education</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico">  
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>   
    <!-- Global CSS -->
    <!-- <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">    -->
    <?php echo $this->Html->css("/assets/plugins/bootstrap/css/bootstrap.min"); ?>
    <!-- Plugins CSS -->    
    <!-- <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css"> -->
    <?php echo $this->Html->css("/assets/plugins/font-awesome/css/font-awesome"); ?>

    <!-- <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css"> -->
    <?php echo $this->Html->css("/assets/plugins/flexslider/flexslider"); ?>
    <!-- <link rel="stylesheet" href="assets/plugins/pretty-photo/css/prettyPhoto.css">  -->
    <?php echo $this->Html->css("/assets/plugins/pretty-photo/css/prettyPhoto"); ?>
    <!-- Responsive Calendar -->
    <?php echo $this->Html->css("/assets/css/responsive-calendar"); ?>
    <!-- Theme CSS -->  
    <?php echo $this->Html->css("/assets/css/styles"); ?>
    <?php echo $this->Html->css("/assets/css/custom"); ?>
    <?php echo $this->Html->css("/assets/css/index"); ?>
    
    <!-- doha CSS -->  
     <?php echo $this->Html->css("/assets/css/sudeb"); ?>
     <?php echo $this->Html->css("/assets/css/doha"); ?>
     <?php echo $this->Html->css("/assets/css/sanjib"); ?>
     <?php echo $this->Html->css("/assets/css/nazim"); ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head> 

<body class="home-page">
    <div class="wrapper">
        <!-- ******HEADER****** --> 
        <header class="header">  
            <div class="header-main container">
                <h1 class="logo col-md-2 col-sm-2">
                    <!-- <a href="index.html"><img id="logo" src="assets/images/logo.png" alt="Logo"></a> -->
                    <a href="index.html"><?php echo $this->Html->image('/assets/images/logo.png',array('id'=>'logo','alt'=>'Logo')); ?></a>
                </h1><!--//logo-->           
                <div class="col-md-10 col-sm-10">
                    <div class="contact">
                        <ul class="list-unstyled list-inline pull-right">
                            <li><a href="#">About</a></li>
                            <li>
                                <ul class="list-unstyled list-inline">
                                    <li><a href="#" class="btn btn-primary">Login</a></li>
                                    <li><input type="text" id="ID" placeholder="ID"></li>
                                    <li><input type="password" id="password" placeholder="Password"></li>
                                    <li><a href="#" class="btn btn-primary">Create Account</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!--//contact-->
                </div><!--//info-->
            </div><!--//header-main-->
        </header><!--//header-->

        <!-- ******NAV****** -->
        <nav class="main-nav" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button><!--//nav-toggle-->
                </div><!--//navbar-header-->            
                <div class="navbar-collapse collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active nav-item"><a href="index.html">Home</a></li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">Courses <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="courses.html">Course List</a></li>
                                <li><a href="course-single.html">Single Course (with image)</a></li>
                                <li><a href="course-single-2.html">Single Course (with video)</a></li>              
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">News <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="news.html">News List</a></li>
                                <li><a href="news-single.html">Single News (with image)</a></li>   
                                <li><a href="news-single-2.html">Single News (with video)</a></li>          
                            </ul>
                        </li>
                        <li class="nav-item"><a href="events.html">Events</a></li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">Pages <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="about.html">About</a></li>
                                <li><a href="team.html">Leadership Team</a></li>
                                <li><a href="jobs.html">Jobs</a></li>
                                <li><a href="job-single.html">Single Job</a></li>
                                <li><a href="gallery.html">Gallery (3 columns)</a></li>
                                <li><a href="gallery-2.html">Gallery (4 columns)</a></li>
                                <li><a href="gallery-3.html">Gallery (2 columns)</a></li>
                                <li><a href="gallery-4.html">Gallery (with sidebar)</a></li>
                                <li><a href="gallery-album.html">Single Gallery</a></li>
                                <li><a href="gallery-album-2.html">Single Gallery (with sidebar)</a></li>
                                <li><a href="faq.html">FAQ</a></li>                                
                                <li><a href="privacy.html">Privacy Policy</a></li> 
                                <li><a href="terms-and-conditions.html">Terms & Conditions</a></li>                   
                            </ul>
                        </li><!--//dropdown-->
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">Shortcodes <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="typography.html"><i class="fa fa-file-text"></i> Typography</a></li>
                                <li><a href="tables.html"><i class="fa fa-table"></i> Tables</a></li>
                                <li><a href="buttons.html"><i class="fa fa-star"></i> Buttons</a></li>
                                <li><a href="components.html"><i class="fa fa-rocket"></i> Components</a></li> 
                                <li><a href="icons.html"><i class="fa fa-heart"></i> Icons</a></li>                                                 
                            </ul>
                        </li><!--//dropdown-->
                        <li class="nav-item"><a href="contact.html">Contact</a></li>
                    </ul><!--//nav-->
                </div><!--//navabr-collapse-->
            </div><!--//container-->
        </nav><!--//main-nav-->
<!-- ******FOOTER****** --> 
    <footer class="footer">
        <div class="bottom-bar">
            <div class="container">
                <div class="row">
                    <small class="copyright col-md-6 col-sm-12 col-xs-12">Copyright @ 2014 College Green Online | Website template by <a href="#">3rd Wave Media</a></small>
                    <ul class="social pull-right col-md-6 col-sm-12 col-xs-12">
                        <li><a href="#" ><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" ><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" ><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#" ><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#" ><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" ><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#" ><i class="fa fa-skype"></i></a></li> 
                        <li class="row-end"><a href="#" ><i class="fa fa-rss"></i></a></li>
                    </ul><!--//social-->
                </div><!--//row-->
            </div><!--//container-->
        </div><!--//bottom-bar-->
    </footer><!--//footer--> 
    <!-- Javascript -->          
    <?php echo $this->Html->script("/assets/plugins/jquery-1.10.2.min"); ?>
    <?php echo $this->Html->script("/assets/plugins/jquery-migrate-1.2.1.min"); ?>
    <?php echo $this->Html->script("/assets/plugins/jquery-migrate-1.2.1.min"); ?>
    <?php echo $this->Html->script("/assets/plugins/bootstrap/js/bootstrap.min"); ?>
    <?php echo $this->Html->script("/assets/plugins/bootstrap-hover-dropdown.min"); ?>
    <?php echo $this->Html->script("/assets/plugins/back-to-top"); ?>
    <?php echo $this->Html->script("/assets/plugins/jquery-placeholder/jquery.placeholder"); ?>
    <?php echo $this->Html->script("/assets/plugins/pretty-photo/js/jquery.prettyPhoto"); ?>
    <?php echo $this->Html->script("/assets/plugins/flexslider/jquery.flexslider-min"); ?>
    <?php echo $this->Html->script("/assets/plugins/jflickrfeed/jflickrfeed.min"); ?>
    <?php echo $this->Html->script("/assets/js/main"); ?>
    <?php echo $this->Html->script("/assets/js/responsive-calendar"); ?>
    <?php echo $this->Html->script("/assets/js/responsive-calendar.min"); ?>
    <script type="text/javascript">
      $(document).ready(function () {
        $(".responsive-calendar").responsiveCalendar({
          time: '2015-01',
          events: {
            "2015-01-14": {}}
        });
      });
    </script>
</body>
</html>
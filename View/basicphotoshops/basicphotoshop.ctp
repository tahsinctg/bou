<!-- ******CONTENT****** --> 
        <div class="content container">
            <div class="container">
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-home"></i></a></li>
              <li class="active"><?php echo __('blog'); ?></li>
            </ol> 
            <div class="row">
                <div class="col-md-12">
                    <h4 class="heading"><?php echo __('Basic Adobe Photoshop'); ?></h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                </div>
                <div class="col-md-9">
                    <div class="blog-view">
                        <div class="course-info-list">
                        <h4 class="heading"><?php echo __('Course Syllabus:'); ?></h4>
                        <ol class="syllabus">
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                            <li>Vivamus consequat urna sit amet enim ullamcorper, ac volutpat arcu molestie.</li>
                            <li>Sed sit amet metus euismod, imperdiet dui facilisis, dapibus nibh.</li>
                            <li>Aliquam maximus nunc eu metus consequat commodo.</li>
                            <li>Nullam vel justo at lorem tincidunt accumsan ac quis justo.</li>
                            <li>Nam vitae dui in velit faucibus gravida.</li>
                            <li>In vitae nulla sed augue eleifend semper sit amet non purus.</li>
                            <li>Sed mattis odio non enim fermentum, sit amet aliquet felis pellentesque.</li>
                            <li>Sed cursus orci vitae facilisis sodales.</li>
                            <li>Nulla viverra nisi ut justo fringilla vehicula.</li>
                            <li>Suspendisse porta nibh ac semper varius.</li>
                            <li>Donec ac mi et sem viverra commodo eget non dolor.</li>
                            <li>Aenean fringilla elit in sem sodales, ac hendrerit arcu pretium.</li>
                        </ol>
                        </div>
                        <div class="course-info-list">
                        <h4 class="heading"><?php echo __('Who It Is For:'); ?></h4>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                        </div>
                        <div class="course-info-list">
                        <h4 class="heading"><?php echo __('Course Type:'); ?></h4>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                        </div>
                        <div class="course-info-list">
                        <h4 class="heading"><?php echo __('Usually Asked Question:'); ?></h4>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                        </div>
                    </div>
                </div>
                <?php echo $this->element('sidebar/rightsidebar'); ?>
            </div>
        </div>  
        </div><!--content-->
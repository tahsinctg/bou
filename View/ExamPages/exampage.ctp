<!-- midcontent start -->
                    <div class="col-md-9">
                        <div class="lesson-title">
                            <div class="question-title">
                                <center> <h2><b>Photoshop Questions Paper</b></h2></center>
                            </div>
                            <h4><b>Adobe Photoshop result out in 18 days</b></h4>
                            <p class=""><h5><i class="fa fa-clock-o fa-2x"></i> <b>00:09:01( Time Remaining )</b></h5></p>
                        </div>
                        <div class="question-section">
                            <div>
                                <div class="question-title">
                                <h4>&nbsp;<i class="glyphicon glyphicon-th-list"></i> <strong>Question No : 1</strong></h4>
                            </div>
                            <h5><b>Which of the following is a common factor of both x2 – 4x – 5 and x2 – 6x – 7?</b></h5>
                            <div class="radio">

                                <label>
                                   <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                       x – 5
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                       x – 7
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                       x – 1
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                      x + 5
                                </label>

                            </div>
                            </div>
                        </div>
                        <div class="question-section">
                            <div class="question-title"><h4>&nbsp;<i class="glyphicon glyphicon-th-list"></i> <strong>Question No : 2</strong></h4></div>
                            <h5><b> IMPREGNABLE : ASSAULT ::</b></h5>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    invincible : control
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    independent : conquest
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    inimitable : modification
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    immutable : alteration
                                </label>
                            </div>
                        </div>
                        <div class="question-section">
                            <div class="question-title"><h4>&nbsp;<i class="glyphicon glyphicon-th-list"></i> <strong>Question No : 3</strong></h4></div>
                            <h5><b>

                                The corporation expects only _______ increases in sales next year despite a yearlong effort to revive its retailing business</b></h5>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    dynamic
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    predictable
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    expanding
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    modest
                                </label>
                            </div>
                        </div>
                        <div class="question-section">
                            <div class="question-title"><h4>&nbsp;<i class="glyphicon glyphicon-th-list"></i> <strong>Question No : 4</strong></h4></div>
                            <h5><b> Approximately what fraction of the workforce in the food service area of the service sector consists of males?</b></h5>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    Design
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    Tools
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    Poster
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    Lecture
                                </label>
                            </div>
                        </div>
                        <div class="question-section">
                            <div class="question-title"><h4>&nbsp;<i class="glyphicon glyphicon-th-list"></i> <strong>Question No : 5</strong></h4></div>
                            <h5><b>

                                In the workforce, the ratio of the number of males to the number of females is the same for the sales sector as it is for the protective service area of the service sector. Which of the following is closest to the number of females in the sales sector?</b></h5>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    2.9 million
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    3.6 million
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    10.4 million
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    11.1 million
                                </label>
                            </div>
                        </div>
                        <div class="question-section">
                            <div class="question-title"><h4>&nbsp;<i class="glyphicon glyphicon-th-list"></i> <strong>Question No : 6</strong></h4></div>
                            <h5><b>

                                Although it does contain some pioneering ideas, one would hardly characterize the work as __________.</b></h5>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    orthodox
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    eccentric
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    original
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="question1" id="optionsRadios1" value="option1" checked>
                                    trifling
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="margin-bottom: 20px;">
                            <div class="col-md-12 text-center">
                                <button type="button" class="btn btn-default btn-primary">Submit Test</button>
                                <button type="button" class="btn btn-default btn-primary">Pause Test</button>
                            </div>
                        </div>
                    </div> <!-- midcontent End -->
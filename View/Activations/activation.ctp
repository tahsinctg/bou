<div class="container" id="min">
            <div class="row">
				<section class="col-md-4 col-md-offset-4" id="activation">
					<div class="input">
						<h3><?php echo __('Activation Page'); ?></h3>
						<form role="form">
							<div class="form-group">
								<label for="exampleInputCode"><?php echo __('Type Code Here:'); ?></label>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="<?php echo __('Code'); ?>">
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-md"><?php echo __('Activate'); ?></button>
						</form>
					</div>
				</section>
			</div>
</div><!--//container-->
<!-- ******CONTENT****** --> 
        <section id="contain">
         	<div class="container">
                
                <div class="row">
                	<div class="col-md-12">
                    	<div class="forum">
                            <h1 class="page-title"><?php echo __('Basic Adobe Photoshop'); ?></h1>
                            <div class="table-responsive">
                            	<table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th colspan="3"><?php echo __('Lesson 1'); ?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        	<p class="sub">Basic Adobe Photoshop</p>
                                      </td>
                                      <td><span class="glyphicon glyphicon-file" aria-hidden="true"></span></td>
                                      <td><span class="glyphicon glyphicon-save" aria-hidden="true"></span></td>
                                    </tr>
                                    <tr>
                                      <td>
                                        	<p class="sub">Basic Adobe Photoshop</p>
                                      </td>
                                      <td><span class="glyphicon glyphicon-book" aria-hidden="true"></span></td>
                                      <td><span class="glyphicon glyphicon-save" aria-hidden="true"></span></td>
                                    </tr>
                                    <tr>
                                      <td>
                                        	<p class="sub">Basic Adobe Photoshop</p>
                                      </td>
                                      <td><span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span></td>
                                      <td><span class="glyphicon glyphicon-save" aria-hidden="true"></span></td>
                                    </tr>
                                    <tr>
                                      <td>
                                        	<p class="sub">Basic Adobe Photoshop</p>
                                      </td>
                                      <td><span class="glyphicon glyphicon-volume-up" aria-hidden="true"></span></td>
                                      <td><span class="glyphicon glyphicon-save" aria-hidden="true"></span></td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                            	<table class="table table-hover">
                                  <thead>
                                    <tr>
                                      <th colspan="3"><?php echo __('Lesson 1'); ?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        	<p class="sub">Basic Adobe Photoshop</p>
                                      </td>
                                      <td><span class="glyphicon glyphicon-file" aria-hidden="true"></span></td>
                                      <td><span class="glyphicon glyphicon-save" aria-hidden="true"></span></td>
                                    </tr>
                                    <tr>
                                      <td>
                                        	<p class="sub">Basic Adobe Photoshop</p>
                                      </td>
                                      <td><span class="glyphicon glyphicon-book" aria-hidden="true"></span></td>
                                      <td><span class="glyphicon glyphicon-save" aria-hidden="true"></span></td>
                                    </tr>
                                    <tr>
                                      <td>
                                        	<p class="sub">Basic Adobe Photoshop</p>
                                      </td>
                                      <td><span class="glyphicon glyphicon-play-circle" aria-hidden="true"></span></td>
                                      <td><span class="glyphicon glyphicon-save" aria-hidden="true"></span></td>
                                    </tr>
                                    <tr>
                                      <td>
                                        	<p class="sub">Basic Adobe Photoshop</p>
                                      </td>
                                      <td><span class="glyphicon glyphicon-volume-up" aria-hidden="true"></span></td>
                                      <td><span class="glyphicon glyphicon-save" aria-hidden="true"></span></td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </section><!--//content-->
    
    <?php echo $this->element('header/header'); ?>
    <div class="content container">
        <section id="contain">
            <div class="container">
                <div class="row">
                    <?php echo $this->element('exampage/sidebar'); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
        </section>
    </div>
</div>


    <?php echo $this->element('footer/footer'); ?>
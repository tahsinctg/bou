<!-- ******CONTENT****** --> 
        <div class="container">
        	<ol class="breadcrumb flat">
                <li><a href="#"><i class="fa fa-home"></i>&nbsp;<?php echo __('Home'); ?></a></li>
                <li><a href="#"><?php echo __('Account Settings'); ?></a></li>
             </ol>
     		<div class="row">
            	<div class="col-md-4">
                    <h2 class="page-title"><?php echo __('Account Settings'); ?></h2>
                	<form role="form">
                    	<div class="form-group">
                        <label for="exampleInputEmail1"><?php echo __('Time Zone'); ?></label>
                    	<select class="form-control">
                          <option><?php echo __('Dhaka'); ?></option>
                          <option><?php echo __('Khulna'); ?></option>
                          <option><?php echo __('Comilla'); ?></option>
                        </select>
                        </div>
                        <div class="form-group">
                        	<button type="submit" class="btn btn-theme"><?php echo __('Time Change'); ?></button>
                        </div>
                      <div class="form-group">
                      	<label for="exampleInputEmail"><?php echo __('Email address Change'); ?></label><br>
                        <label for="exampleInputEmail1"><?php echo __('New Email address'); ?></label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                      </div>
                      <div class="form-group">
                        	<button type="submit" class="btn btn-theme"><?php echo __('Email Change'); ?></button>
                        </div>
                      <div class="form-group">
                      	<label for="exampleInputPassword"><?php echo __('Password Change'); ?></label><br>
                        <label for="exampleInputPassword1"><?php echo __('Old Password'); ?></label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="old Password">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1"><?php echo __('New Password'); ?></label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="new Password">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1"><?php echo __('confirm Password'); ?></label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="confirm Password">
                      </div>
                      <div class="form-group">
                        	<button type="submit" class="btn btn-theme"><?php echo __('Password Change'); ?></button>
                        </div>
                        
                    </form>
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div><!-- content -->